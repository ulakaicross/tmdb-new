export const params = {
    apiKey : "ee25b2dc7b39cf02eb4b0848345bd05a",
    imagePath : "https://image.tmdb.org/t/p/original",
    nowPlayingVal : "https://api.themoviedb.org/3/movie/now_playing?api_key=",
    setGenreVal : "https://api.themoviedb.org/3/genre/movie/list?api_key=",
    singleMovie : "https://api.themoviedb.org/3/movie/",
    apiQueryString : "?api_key=",
    setLangVal : "&language=en-US",
    pageQuery : "&page=",
    defaultPage : 1,
    defaultFilterValue : 3,
}

export const defaults = {
    nowPlaying: params.nowPlayingVal + params.apiKey + params.setLangVal + params.pageQuery + params.defaultPage,
    setDefaultPage: params.setGenreVal + params.apiKey + params.setLangVal + params.pageQuery + params.defaultPage,
    setPage: params.nowPlayingVal + params.apiKey + params.setLangVal + params.pageQuery + params.pageQuery
}
