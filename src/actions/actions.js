import { ADD_TO_WATCHLIST } from "./actiontypes";

let nextMovieId = 0;

export const addToWatchlist = (movieId, movie) => {
    return({
    type: ADD_TO_WATCHLIST,
    payload: {
        id: ++nextMovieId,
        movieId: movieId,
        movie: movie
    }
    
})};