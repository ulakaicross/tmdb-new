import React from "react";

const Pagination = (props) => {
  const returnPage = (pageNum) => {
    if(pageNum > 0 && pageNum <= props.pages) {
        return(pageNum)
    } else {
        return (props.page)
    }
  }

  return (
    <form>
      <button data-page={returnPage(props.page-1)} onClick={props.onClick}>Prev</button>
      <button data-page={returnPage(props.page+1)} onClick={props.onClick}>Next</button>
      <span>Page {props.page} of {props.pages}</span>
    </form>
  );
};

export default Pagination;
