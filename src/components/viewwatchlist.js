import React from "react";

const ViewWatchlist = (props) => {

  const click = (e) => {
    e.preventDefault();
    props.onClick(props.value);
  };

  return (
    <>
      {props.showWatchList ? (
        <button
          onClick={(e) => {
            click(e);
          }}
        >
          Hide watch list
        </button>
      ) : (
        <button
          onClick={(e) => {
            click(e);
          }}
        >
          View watch list
        </button>
      )}
    </>
  );
};

export default ViewWatchlist;
