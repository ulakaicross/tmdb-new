import React from "react";
import ViewWatchList from "./viewwatchlist";
import { params } from "../constants/constants"

const Filter = (props) => {

  const defaultValue = params.defaultFilterValue;
  const items = [];

  for (let i = 10; i >= 0; i = i - 0.5) {
    if (i === defaultValue) {
      items.push(
        <option value={defaultValue} key={i}>
          {i}
        </option>
      );
    } else {
      items.push(
        <option value={i} key={i}>
          {i}
        </option>
      );
    }
  }

  return (
    <form className="frm frm-header" onChange={props.onChange}>
      <label className="lbl">Sort by rating &nbsp;</label>
      <select defaultValue={defaultValue} className="slct">
        {items}
      </select>
      <span className="lbl-spn"> and above</span>
      <span><ViewWatchList onClick={props.onClick} showWatchList={props.showWatchList}/></span>
    </form>
  );
};

export default Filter;
