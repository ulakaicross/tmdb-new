import React, { useState } from "react";
import { addToWatchlist } from "../actions/actions"
import { useDispatch } from "react-redux";
import fetchMovie from "../helpers/fetchmoviedetails";
import store from "../state/store"


const AddToWatchList = props => {
    const [clickButton, setClickButton] = useState(null);
    const dispatch = useDispatch();

    const setMouseOver = (e) => {
        setClickButton(e.target.dataset.movieId);
    }

    const setOnClick = async (click) => {
        const movie =  await fetchMovie(click);
        props.onClick(props.value);
        dispatch(addToWatchlist(click, movie));
    }

    return (
        <div>
            <button data-movie-id={props.movieId} onMouseOver={e => setMouseOver(e)} onClick={() => setOnClick(clickButton)}>Add to watch list</button>
        </div>
    )

}

export default AddToWatchList;