import React, { useState } from "react";
import verifyStore from "../helpers/verifywishlist";
import AddToWatchlist from "./addtowatchlistbutton";

const Card = (props) => {

  const [wishlist, setWishlist] = useState(false);
  const [inStore] = useState(verifyStore(props.movieId))
  
  const addGenres = (genreId) => {
    let result = props.genreList.filter((obj) => {
      return obj.id === genreId;
    });
    return result[0].name;
  };
  
  return (
    <div className="card-container col-xs-12 col-lg-6 col-xl-4">
      <div className="card-item row">
        <div className="image-container col-xs-12 col-lg-6">
          <img className="img img-card" src={props.src} alt="" />
        </div>
        <div className="info-container col-xs-12 col-lg-6">
          <h2>{props.title}</h2>
          <h3>Synopsis:</h3>
          <p>{props.overview}</p>
          <h3>Rating: {props.rating} / 10</h3>
          <h3>
            Genres:
            <br />
            {props.genres.map((item) => {
              return (
                <span key={item} className="info-spn">
                  {addGenres(item)}
                </span>
              );
            })}
          </h3>
        </div>{
          inStore || wishlist ? (<span>Movie in watch list</span>) : (<AddToWatchlist movieId={props.movieId} onClick={() => setWishlist(true)}/>)
        }
        
      </div>
    </div>
  );
};

export default Card;
