import { createStore } from "redux";
import watchList from "./reducers/addtowatchlist"

export default createStore(watchList);