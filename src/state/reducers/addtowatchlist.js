import { ADD_TO_WATCHLIST } from "../../actions/actiontypes";

const initialState = [];

const movieList = (state = initialState, action) => {
    console.log(action)
    switch (action.type) {
        case ADD_TO_WATCHLIST: {
            const { id, movieId, movie } = action.payload;
            return { 
                ...state,
            [id]: {
                    movieId: movieId,
                    movie: movie
                }
            }
        }
        default: return state;
    }
}

export default movieList;