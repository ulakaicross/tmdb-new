import { params } from "../constants/constants";

const fetchMovie = async (id) => {

    const url = params.singleMovie+id+params.apiQueryString+params.apiKey;
    const result = await fetch(url);
    const json = await result.json();
    return json;
}

export default fetchMovie;