import store from "../state/store"

const buildWatchlist = () => {
    const wishlistArr = Object.keys(store.getState()).map(i => store.getState()[i])
    const watchList = [];
    wishlistArr.forEach(movie => {
        watchList.push(movie.movie);
        
    })
    return watchList;
}
export default buildWatchlist