import store from "../state/store"

const verifyStore = (movieId) => {
    const wishlistArr = Object.keys(store.getState()).map(i => store.getState()[i])
    let inStore = false;
    wishlistArr.forEach(movie => {
        let storeId = parseInt(movie.movieId);
        if (storeId === movieId) inStore = true;
    })
    return inStore;    
}

export default verifyStore;