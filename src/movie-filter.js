import React, { useState, useEffect } from "react";
import Filter from "./components/filter";
import Card from "./components/card";
import Pagination from "./components/pagination";
import { params } from "./constants/constants";
import { defaults } from "./constants/constants";
import buildWatchlist from "./helpers/buildwatchlist";

const HookMovieFilter = () => {
  const [filterValue, setFilterValue] = useState(params.defaultFilterValue);
  const [movies, setMovies] = useState({});
  const [pages, setPages] = useState({});
  const [page, setPage] = useState({});
  const [genre, setGenre] = useState({});
  const [loaded1, setLoaded1] = useState(false);
  const [loaded2, setLoaded2] = useState(false);
  const [moviesList, setMoviesList] = useState({});
  const [viewWatchList, setViewWatchList] = useState(false);
  const nowPlaying = defaults.nowPlaying;
  const setDefaultPage = defaults.setDefaultPage;
  const selectPage = defaults.setPage;

  const fetchData = async (url, flag) => {
    try {
      const result = await fetch(url);
      const json = await result.json();
      switch (flag) {
        case 0:
          setMovies(json.results);
          setPages(json.total_pages);
          setPage(json.page);
          break;
        case 1:
          setGenre(json.genres);
          break;
        default:
          console.warn("switch defaulted");
      }
    } catch (e) {
      console.warn(e);
    }
  };

  useEffect(() => {
    fetchData(
       nowPlaying, 0
    ).then(() => {
      setLoaded1(true);
    });
    fetchData(
       setDefaultPage, 1
    ).then(() => {
      setLoaded2(true);
    });
  }, [nowPlaying, setDefaultPage]);

  useEffect(() => {
    if (movies.length) {
      setMoviesList(() => {
        let filtered = movies
          .filter(result => result.vote_average >= filterValue)
          .sort((a, b) => b.vote_average - a.vote_average);
        return filtered;
      });
    }
  }, [filterValue, movies]);

  const fetcWatchlist = () => {
    const watchList = buildWatchlist();
    if(watchList.length) {
      setViewWatchList(!viewWatchList);
      if(!viewWatchList) {
        setMovies(watchList); 
      } else {
        fetchData(selectPage+page, 0);
      }
    }
  }

  // Fix for inconsistency between movie list and movie item genre object literal

  const fixGenres = (genres, movie) => {
    const normalisedGenres = [];
    if (genres) {
      return genres;
    } else {
      movie.genres.forEach(genre => {
        normalisedGenres.push(genre.id)
      })
      return normalisedGenres;
    }
  };

  return (
    <div className="app">
      <header className="hdr">
        <h1>Out Now!</h1>
      <Filter
        onChange={e => {
          setFilterValue(e.target.value);
        }}
         onClick={() => fetcWatchlist()}
         showWatchList={viewWatchList}
      />
      </header>
      <div className="film-card col-xs-12">
        <div className="row">
          {loaded1 && loaded2 ? (
            moviesList.map(movie => (
              <Card
                key={movie.id}
                src={params.imagePath + movie.poster_path}
                movieTitle={movie.title}
                overview={movie.overview}
                rating={movie.vote_average}
                genres={fixGenres(movie.genre_ids, movie)}
                genreList={genre}
                movieId={movie.id}
              />
            ))
          ) : (
            <p>Loading...</p>
          )}
        </div>
      </div>
      <div className="row">
      {loaded1 && loaded2 ? (
        <Pagination pages={pages} page={page} onClick={e => {
          e.preventDefault();
          fetchData(selectPage+e.target.dataset.page, 0)
          }}/>
      ) : (<span></span>)}
      </div>
    </div>
  );
};

export default HookMovieFilter;
